importScripts("https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/5.5.5/firebase-messaging.js");

// Initialize Firebase
var config = {
  apiKey: "AIzaSyB-wL6SBwEzK_YTqETTF5dHm1A5IqY8FmU",
  authDomain: "webpushnotification-13dd2.firebaseapp.com",
  databaseURL: "https://webpushnotification-13dd2.firebaseio.com",
  projectId: "webpushnotification-13dd2",
  storageBucket: "",
  messagingSenderId: "1023178756870"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging
  .setBackgroundMessageHandler((payload) => {
    const title = 'Hello World';
    const options = {
      body: payload.data.status
    };
    return self.registration.showNotification(title, options);
  });