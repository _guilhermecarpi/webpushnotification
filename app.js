// Initialize Firebase
let config = {
  apiKey: "AIzaSyB-wL6SBwEzK_YTqETTF5dHm1A5IqY8FmU",
  authDomain: "webpushnotification-13dd2.firebaseapp.com",
  databaseURL: "https://webpushnotification-13dd2.firebaseio.com",
  projectId: "webpushnotification-13dd2",
  storageBucket: "",
  messagingSenderId: "1023178756870"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging
  .requestPermission()
  .then(() => {
    console.log('Permission granted!');
    return messaging.getToken();
  })
  .then((token) => {

    subscribeToTopic(token, 'comments');

  })
  .catch((err) => {
    console.log('Permission denied: ', err);
  });

messaging
  .onMessage((payload) => {
    console.log('Notification received: ', payload);
  });

messaging.onTokenRefresh(() => {
  messaging
    .getToken()
    .then((token) => {
      console.log('Token refreshed: ', token);
    }).catch(function (err) {
      console.log('Unable to retrieve refreshed token: ', err);
    });
});

function subscribeToTopic(token, topic) {

  let headers = {
    'method': 'POST',
    'headers': new Headers({
      'Authorization': 'key=AAAA7jo00wY:APA91bHPVYalh8Tb0D1E__o44ZWtAp9Pc4hSdV7NcLCJUgwjS30HrxkczUmTR_KtpU2Fg_sEyznkw5agmIZfFI4YUZKo_18xYCLKkGpvKTl4g6YRZInyQCgqYMtlPnmMfCs1DldyPKaK', 
      'Content-Type': 'application/json'
    })
  };

  fetch(`https://iid.googleapis.com/iid/v1/${token}/rel/topics/${topic}`, headers)
    .then((response) => {
      console.log('Fetch success', response);
    }).catch((err) => {
      console.log('Fetch error: ', err);
    });
}